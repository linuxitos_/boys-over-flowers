<?php

function timeago($date){
	if(empty($date)) {
		return "Fecha no proporcionada.";
	}
	
	$periods 	= array("seg", "min", "hora", "día", "semana", "mes", "año", "decada");
	$lengths 	= array("60","60","24","7","4.35","12","10");
	$now 		= time();
	$unix_date 	= strtotime($date);
	
	   // check validity of date
	if(empty($unix_date)) {    
		return "Error en la fecha";
	}

	// is it future date or past date
	if($now > $unix_date) {    
		$difference     = $now - $unix_date;
		$tense         = "hace";
		
	} else {
		$difference     = $unix_date - $now;
		$tense         = "justo ahora";
	}
	
	for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
		$difference /= $lengths[$j];
	}
	
	$difference = round($difference);
	
	if($difference != 1) {
		if ($periods[$j]=="mes") {
			$periods[$j].= "es";
		}else{
			$periods[$j].= "s";
		}
	}
	
	return "{$tense} $difference $periods[$j]";
}


function mesDiaAnio($fecha){
	$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
	$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	return $meses[date("n", strtotime($fecha))-1]." ".date("d", strtotime($fecha)). ", ".date(date("Y", strtotime($fecha))) ;
}