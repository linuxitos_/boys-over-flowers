<?php

namespace App\Controllers;

use App\Models\Post;

class Home extends BaseController
{
    protected $helpers = [];
	protected $db;
    protected $mPost;

	public function __construct(){
		helper(['url', 'session', 'emai', 'upload', 'date_helper', 'database', 'form']);
		$this->db = $db = \Config\Database::connect();
        $this->mPost = new Post();
	}

    public function index(): string
    {
        $data['post'] = $this->mPost->select('fc_post, nom_post')
        ->where('user_id', 1)
        ->where('type_post', 'evt')
        ->orderBy('fc_post', 'asc')
        ->get()
	    ->getRow();
        $data['title'] 	= 'Home  | linuxitos';
		$data['tab'] 	= 'home';
		return view('main', $data);
    }

    public function addEvt() {
        $add = [
            'fc_post' => $this->request->getVar('txtFechaEvt') ? trim($this->request->getVar('txtFechaEvt')) : '',
            'nom_post' => $this->request->getVar('txtDescEvt') ? trim($this->request->getVar('txtDescEvt')) : '',
            'type_post' => 'evt',
            'user_id' => 1,
		];
		$rules = [
			'txtDescEvt' 	=> 'required|min_length[3]|max_length[100]',
			'txtFechaEvt' 	=> 'required|min_length[3]|max_length[200]',
		];

		$messages = [
			"txtDescEvt" => [
				"required" => "La descripción es requerida",
				"min_length" => "Mínimo 3 caracteres",
				"max_length" => "Máximo 100 caracteres",
			],
			"txtFechaEvt" => [
				"required" => "La fecha es requerida",
				"min_length" => "Mínimo 3 caracteres",
				"max_length" => "Máximo 200 caracteres",
			]
		];
		if (!$this->validate($rules, $messages)) {
			$errors = $this->validator->getErrors();
			$msgs 	= "";
			foreach ($errors as $error) {
				$msgs .= esc($error) . '<br>';
			}
			$msg = array(
                "type" => 'error',
                "icon" 	=> 'bi bi-check-circle',
                "msg" 	=> $msgs
            );
		} else {
			if (($aPo = $this->mPost->insert($add))) {
                $msg = array(
                    "type" => 'success',
                    "icon" 	=> 'bi bi-check-circle',
                    "msg" 	=> 'Registro añadido correctamente.'
                );
            }else{
                $msg = array(
                    "type" => 'error',
                    "icon" 	=> 'bi bi-exclamation-circle',
                    "msg" 	=> 'Error interno, intenta más tarde.'
                );
            }
        }
        
    
        return $this->response->setJSON($msg);
    }

    public function loadEvts() {
        $post = $this->mPost->select('fc_post, nom_post')
        ->where('user_id', 1)
        ->where('type_post', 'evt')
        ->orderBy('fc_post', 'asc')
        ->get()
	    ->getRow();
        $response['results'] = "";
		if ($post) {
			$response['results'] .= '<li class="list-group-item d-flex justify-content-between align-items-center text-dark">
                    '.$post->nom_post.'
                    <span class="badge bg-primary rounded-pill">'.mesDiaAnio($post->fc_post).'</span>
                </li>';
		} else {
			$response['results'] = 'Sin resultados';
		}
		return $this->response->setJSON($response);
    }

    public function addFel() {
        $add = [
            'fc_post' => $this->request->getVar('txtFechaFel') ? trim($this->request->getVar('txtFechaFel')) : '',
            'nom_post' => $this->request->getVar('txtNomFel') ? trim($this->request->getVar('txtNomFel')) : '',
            'desc_post' => $this->request->getVar('txtDescFel') ? trim($this->request->getVar('txtDescFel')) : '',
            'type_post' => 'fel',
            'user_id' => 1,
		];
		$rules = [
			'txtNomFel' 	=> 'required|min_length[3]|max_length[100]',
			'txtFechaFel' 	=> 'required|min_length[3]|max_length[200]',
		];

		$messages = [
			"txtNomFel" => [
				"required" => "La descripción es requerida",
				"min_length" => "Mínimo 3 caracteres",
				"max_length" => "Máximo 100 caracteres",
			],
			"txtFechaFel" => [
				"required" => "La fecha es requerida",
				"min_length" => "Mínimo 3 caracteres",
				"max_length" => "Máximo 200 caracteres",
			]
		];
		if (!$this->validate($rules, $messages)) {
			$errors = $this->validator->getErrors();
			$msgs 	= "";
			foreach ($errors as $error) {
				$msgs .= esc($error) . '. ';
			}
			$msg = array(
                "type" => 'error',
                "icon" 	=> 'bi bi-check-circle',
                "msg" 	=> $msgs
            );
		} else {
			if (($aPo = $this->mPost->insert($add))) {
                $msg = array(
                    "type" => 'success',
                    "icon" 	=> 'bi bi-check-circle',
                    "msg" 	=> 'Registro añadido correctamente.'
                );
            }else{
                $msg = array(
                    "type" => 'error',
                    "icon" 	=> 'bi bi-exclamation-circle',
                    "msg" 	=> 'Error interno, intenta más tarde.'
                );
            }
        }
        $msg = array(
            "status" => 'nada',
            "type" 	=> 'error',
            "msg" 	=> 'Ya tu sabrás... 🖕🏻'
        );
    
        return $this->response->setJSON($msg);
    }

    public function loadFels() {
        $posts = $this->mPost->select('id_post, fc_post, nom_post, desc_post')
        ->where('user_id', 1)
        ->where('type_post', 'fel')
        ->orderBy('fc_post', 'asc')
        ->get()
	    ->getResult();
        $response['results'] = "";
		if ($posts) {
            foreach ($posts as $post) {
			    $response['results'] .= '
                <div class="col-md-3 mb-3">
                    <div class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
                        <div class="toast-header">
                            <strong class="me-auto">'.$post->nom_post.'</strong>
                            <small title="'.$post->fc_post.'">'.timeago($post->fc_post).'</small>
                            <button id="btnDelFel'.$post->id_post.'" type="button" title="Eliminar" class="btn btn-link ms-2 mb-1 p-0 btnDelFel" data-fel="'.$post->id_post.'">
                                <i class="bi bi-trash-fill text-danger"></i>
                            </button>
                        </div>
                        <div class="toast-body">
                            '.$post->desc_post.'
                        </div>
                    </div>
                </div>';
            }
		} else {
			$response['results'] = '<p class="text-center">Sin felicitaciones agregadas</p>';
		}
		return $this->response->setJSON($response);
    }

    public function delFel() {
        $del = [
            'id_post' => $this->request->getVar('fel') ? trim($this->request->getVar('fel')) : 0,
		];	
        if ($this->mPost->where('id_post', $del['id_post'])->delete()) {
            $msg = array(
                "type" => 'success',
                "icon" 	=> 'bi bi-check-circle',
                "msg" 	=> 'Registro eliminado.'
            );
        }else{
            $msg = array(
                "type" => 'error',
                "icon" 	=> 'bi bi-exclamation-circle',
                "msg" 	=> 'Error interno, intenta más tarde.'
            );
        }
    
        return $this->response->setJSON($msg);
    }

    public function sendCode() {
        $code = $this->request->getVar('code') ? trim($this->request->getVar('code')) : '';
        $msg = array(
            "type" => 'success',
            "icon" 	=> 'bi bi-check-circle',
            "msg" 	=> 'Verificación correcta.'
        );
    
        return $this->response->setJSON($msg);
    }
}
