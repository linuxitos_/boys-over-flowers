<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');


$routes->match(['get', 'post'], 'addEvt', 'Home::addEvt');
$routes->match(['get', 'post'], 'addFel', 'Home::addFel');
$routes->match(['get', 'post'], 'loadEvts', 'Home::loadEvts');
$routes->match(['get', 'post'], 'loadFels', 'Home::loadFels');
$routes->match(['get', 'post'], 'delFel', 'Home::delFel');
$routes->match(['get', 'post'], 'sendCode', 'Home::sendCode');
