<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h1 class="modal-title fs-5" id="exampleModalLabel">Actualizando evento</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form class="formAddEvt" method="post" enctype="multipart/form-data"  accept-charset="utf-8">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="input-group">
								<span class="has-float-label">
									<input type="text" class="form-control" name="txtDescEvt" id="txtDescEvt" placeholder=" " required="required" maxlength="200">
									<label for="txtDescEvt">Descripción</label>
									<i class="bi bi-journal icon-inside"></i>
								</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group mb-3">
								<input type="date" name="txtFechaEvt" id="txtFechaEvt" class="form-control" placeholder=" " aria-describedby="basic-addon1">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer p-2">
					<button id="bntCloseAddEvt" type="button" class="btn btn-secondary" data-bs-dismiss="modal">
						<i class="bi bi-x-circle"></i> Cerrar
					</button>
					<button id="btnAddEvt" type="submit" class="btn btn-primary">
						<i class="bi bi-check-circle"></i> Actualizar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="addFel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h1 class="modal-title fs-5">Agregando felicitación</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form class="formAddFel" method="post" enctype="multipart/form-data"  accept-charset="utf-8">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="input-group">
								<span class="has-float-label">
									<input type="text" class="form-control" name="txtNomFel" id="txtNomFel" placeholder=" " required="required" maxlength="300">
									<label for="txtNomFel">Evento</label>
									<i class="bi bi-journal icon-inside"></i>
								</span>
							</div>
						</div>
						<div class="col-md-12">
							<div class="input-group">
								<span class="has-float-label">
									<input type="text" class="form-control" name="txtDescFel" id="txtDescFel" placeholder=" " required="required" maxlength="300">
									<label for="txtDescFel">Descripción</label>
									<i class="bi bi-journal icon-inside"></i>
								</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group mb-3">
								<input type="date" name="txtFechaFel" id="txtFechaFel" class="form-control" placeholder=" " aria-describedby="basic-addon1">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer p-2">
					<button id="bntCloseAddFel" type="button" class="btn btn-secondary" data-bs-dismiss="modal">
						<i class="bi bi-x-circle"></i> Cerrar
					</button>
					<button id="btnAddFel" type="submit" class="btn btn-success">
						<i class="bi bi-check-circle"></i> Agregar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="mdlOtp" tabindex="-1" aria-labelledby="sss" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header p-2">
				<h1 class="modal-title fs-5" id="sss">
					<i class="bi bi-shield-check"></i> OTP Validation
				</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<h1 class="display-2 text-center">
							<i class="bi bi-shield-check"></i>
						</h1>
						<p class="display-7 text-center">
							Enter OTP code
						</p>
					</div>
				</div>
				<form id="otpForm" class="formOtp" method="post" enctype="multipart/form-data"  accept-charset="utf-8">
					<div class="justify-content-center align-items-center">
						<div id="divCntOtp"></div>
						<div id="otp" class="inputs d-flex justify-content-center">
							<input class="m-2 text-center form-control rounded txtOtp" type="text" id="first" maxlength="1" autocomplete="off"/>
							<input class="m-2 text-center form-control rounded txtOtp" type="text" id="second" maxlength="1" autocomplete="off"/>
							<input class="m-2 text-center form-control rounded txtOtp" type="text" id="third" maxlength="1" autocomplete="off"/>
							<input class="m-2 text-center form-control rounded txtOtp" type="text" id="fourth" maxlength="1" autocomplete="off"/>
							<input class="m-2 text-center form-control rounded txtOtp" type="text" id="fifth" maxlength="1" autocomplete="off"/>
							<input class="m-2 text-center form-control rounded txtOtp" type="text" id="sixth" maxlength="1" autocomplete="off"/>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-center">
							<button id="btnNewCode" type="button" class="btn btn-link">
								<i class="fa-thin fa-arrow-rotate-left"></i> Solicitar nuevo código
							</button>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button id="bntCloseMdlOtp" type="button" class="btn btn-secondary" data-bs-dismiss="modal">
					<i class="bi bi-x-circle"></i> Cerrar
				</button>
			</div>
		</div>
	</div>
</div>