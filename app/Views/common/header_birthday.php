<!doctype html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?= base_url('assets/app/images/fav.png'); ?>">
	<title><?= $title; ?></title>

	<!-- Bootstrap core CSS-->
	<link href="<?= base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendor/bootstrap-icons/bootstrap-icons.min.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendor/bootstrap/css/animate.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendor/bootstrap/css/animation.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendor/bootstrap/css/fileinput.css'); ?>" media="all" rel="stylesheet" type="text/css" />
	<link href="<?= base_url('assets/vendor/font-awesome/css/all.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('assets/app/css/app.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/app/css/posts.css'); ?>" rel="stylesheet">
	<script src="<?= base_url('assets/vendor/jquery/jquery-3.6.3.min.js'); ?>"></script>

	<!-- For owl slider-->
	<link rel="stylesheet" href="<?= base_url('/assets/vendor/owlcarousel/owl.carousel.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('/assets/vendor/owlcarousel/owl.theme.default.min.css'); ?>">
	<!-- For owl slider-->
	<link rel="stylesheet" href="<?= base_url('/assets/vendor/prism/code.css'); ?>">

	<!--script src='https://www.google.com/recaptcha/api.js'></script-->
	<meta name="description" content="Tips sobre linux, Hacks, Tutoriales, Desarrollo Web e Ideas en un sólo Blog">
	<meta name="author" content="Fernando Merino">
	<meta name="keywords" content="linuxitos,fedora,development,blog,m2">
  <style>
      body {
      margin: 0;
      background: #020202;
      cursor: crosshair;
      }
      canvas{display:block}
      h1 {
		position: absolute;
		top: 20%;
		left: 50%;
		transform: translate(-50%, -50%);
		color: #fff;
		font-family: "Source Sans Pro";
		font-size: 5em;
		font-weight: 900;
		-webkit-user-select: none;
		user-select: none;
      }
	  h6 {
		position: absolute;
		top: 30%;
		left: 50%;
		transform: translate(-50%, -50%);
		color: #fff;
		font-family: "Source Sans Pro";
		font-weight: 900;
		-webkit-user-select: none;
		user-select: none;
      }
  </style>
</head>

<body>