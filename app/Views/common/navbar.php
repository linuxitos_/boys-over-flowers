<?php

?>
<header class="p-3 mb-3 border-bottom">
	<div class="container">
		<div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
			<a href="/" class="d-flex align-items-center mb-2 mb-lg-0 link-body-emphasis text-decoration-none">
				<img class="img-fluid" src="<?= base_url('public/assets/images/logo.svg'); ?>" id="logo_custom" alt="logo">
			</a>

			<ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
				<li><a href="#" class="nav-link px-2 link-secondary">Inicio</a></li>
			</ul>

			<form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3" role="search">
				<input type="search" class="form-control" placeholder="Buscar..." aria-label="Search">
			</form>

			<div class="dropdown text-end">
				<a href="#" class="d-block link-body-emphasis text-decoration-none dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
					<img src="<?= base_url('public/assets/images/user.png'); ?>" alt="mdo" width="32" height="32" class="rounded-circle">
				</a>
				<ul class="dropdown-menu dropdown-menu-end text-small">
					<li>
						<a class="dropdown-item" href="#">
							Mi perfil <i class="bi bi-person float-end"></i>
						</a>
					</li>
					<li>
						<a class="dropdown-item" href="#">
							Mis cosas <i class="bi bi-puzzle float-end"></i>
						</a>
					</li>
					<li><hr class="dropdown-divider"></li>
					<li>
						<a class="dropdown-item" href="#">
							Salir <i class="bi bi-box-arrow-right float-end"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</header>