<?php

use App\Controllers\Blog;
?>
<div class="card">
	<div class="card-header p-1 text-center">
		Entradas del blog
	</div>
	<nav class="bd-links" id="bd-docs-nav" aria-label="Docs navigation">
		<ul class="list-unstyled mb-0 p-0 m-0 py-3 pt-md-1">
			<?php
			$years = Blog::getYears();
			foreach ($years as $y) {
			?>
				<li class="pb-0 pt-0 mt-0 mb-0">
					<button class="btn btn-link text-decoration-none d-inline-flex pb-1 pt-1 mt-1 mb-1 align-items-center rounded collapsed anios anio-desglose" data-bs-toggle="collapse" data-bs-target="#dydleft<?= $y->anio; ?>" aria-expanded="false" aria-current="true" data-div="dydleft" data-month="0" data-year="<?= $y->anio; ?>">
						<?= $y->anio; ?> (<?= Blog::countTotalPostByYear($y->anio) ?>)
					</button>
					<div class="collapse" id="dydleft<?= $y->anio; ?>">

					</div>
				</li>
			<?php
			}
			?>
		</ul>
	</nav>
</div>

<script type="text/javascript">
	$(document).on("click", ".anio-desglose", function() {
		var year = $(this).data('year');
		var month = $(this).data('month');
		var div = $(this).data('div');
		if ($(this).not(".collapsed")) {
			$.ajax({
				type: "POST",
				data: {
					year: year,
					month: month,
					div: div,
				},
				url: base_url + '/months',
				dataType: 'JSON',
				beforeSend: function(objeto) {
					$("#" + div + year).html('<div class="col-md-12 m-1">' +
						'<span class="spinner-own spin-1x" role="status" aria-hidden="true"></span> <span>Cargando...</span></div>');
				},
				success: function(responseData) {
					$("#" + div + year).html('');
					$("#" + div + year).append(responseData.search);
				},
				error: function(response) {
					$("#" + div + year).html('<div class="col-md-12 p-0"><div class="p-1 alert alert-danger alert-dismissible text-center" role="alert"><strong><i class="fa fa-exclamation-circle"></i></strong>' +
						' Error interno, intenta más tarde.</div></div>');
				}
			});
		}
	});

	$(document).on("click", ".month-desglose", function() {
		var year = $(this).data('year');
		var month = $(this).data('month');
		var div = $(this).data('div');
		if ($(this).not(".collapsed")) {
			$.ajax({
				type: "POST",
				data: {
					year: year,
					month: month,
					div: div,
				},
				url: base_url + '/postsMonth',
				dataType: 'JSON',
				beforeSend: function(objeto) {
					$("#" + div).html('<div class="col-md-12">' +
						'<span class="spinner-own spin-1x" role="status" aria-hidden="true"></span> <span>Cargando...</span></div>');
				},
				success: function(responseData) {
					$("#" + div).html('');
					$("#" + div).append(responseData.search);
				},
				error: function(response) {
					$("#" + div).html('<div class="col-md-12 p-0"><div class="p-1 alert alert-danger alert-dismissible text-center" role="alert"><strong><i class="fa fa-exclamation-circle"></i></strong>' +
						' Error, intenta más tarde.</div></div>');
				}
			});
		}
	});
</script>

<div class="card mt-3">
	<div class="card-header p-1 text-center">
		Conoce la red social linuxClick
	</div>
	<div class="card-body p-2">
		<a href="https://redlinuxclick.com/" target="_blank">
			<img class="figure-img d-block img-fluid w-100 mb-0" src="<?= base_url('assets/app/images/lclogofull.png') ?>">
		</a>
	</div>
</div>

<div class="card mt-4">
	<!--iframe id='kofiframe' src='https://ko-fi.com/linuxitos/?hidefeed=true&widget=true&embed=true&preview=true' style='border:none;width:100%;padding:4px;background:#f9f9f9;' height='712' title='linuxitos'></iframe-->
	<script src='https://storage.ko-fi.com/cdn/scripts/overlay-widget.js'></script>
	<script>
		kofiWidgetOverlay.draw('linuxitos', {
			'type': 'floating-chat',
			'floating-chat.donateButton.text': 'Support me',
			'floating-chat.donateButton.background-color': '#00b9fe',
			'floating-chat.donateButton.text-color': '#fff'
		});
	</script>
</div>

<div class="card mt-4">
	<div class="card-body p-2 text-center">
		<div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v5.0&appId=452152535336853"></script>
		<div class="fb-page" data-href="https://www.facebook.com/linuxitos/" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
			<blockquote cite="https://www.facebook.com/linuxitos/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/linuxitos/">LiNuXiToS</a></blockquote>
		</div>
	</div>
</div>
<div class="card mt-4">
	<div class="card-header p-1 text-center">
		Redes sociales
	</div>
	<div class="card-body p-2 text-center">
		<a href="https://www.facebook.com/linuxitos/" target="_blank" class="btn-social btn-facebook"><i class="fab fa-facebook"></i></a>
		<a href="https://www.instagram.com/linuxitos/" target="_blank" class="btn-social btn-instagram"><i class="fab fa-instagram"></i></a>
		<a href="https://github.com/jesusferm/" target="_blank" class="btn-social btn-github"><i class="fab fa-github-alt"></i></a>
		<a href="https://www.linkedin.com/in/jfernandomerino" target="_blank" class="btn-social btn-linkedin"><i class="fab fa-linkedin"></i></a>
		<a href="https://twitter.com/linuxitos_" target="_blank" class="btn-social btn-twitter"><i class="fab fa-twitter"></i></a>
		<a href="https://www.youtube.com/@linuxitos" target="_blank" class="btn-social btn-youtube"><i class="fab fa-youtube"></i></a>
		<a href="https://t.me/linuxitos" target="_blank" class="btn-social btn-telegram"><i class="fab fa-telegram-plane"></i></a>
	</div>
</div>

<div class="card mt-3 mb-3">
	<div class="card-header p-1 text-center">
		Accesos directos
	</div>
	<div class="card-body p-2">
		<a href="https://getfedora.org" target="_blank">
			<img class="figure-img d-block img-fluid w-100 mb-0" src="<?= base_url('assets/app/images/fedora_logo.svg') ?>">
		</a>
		<div class="dropdown-divider"></div>
		<a href="https://gnome-look.org" target="_blank">
			<img class="figure-img d-block img-fluid w-100 mb-0" src="<?= base_url('assets/app/images/gnome-look-logo-2.png') ?>">
		</a>
		<div class="dropdown-divider"></div>
		<a href="https://fedoramagazine.org" target="_blank">
			<img class="figure-img d-block img-fluid w-100 mb-0" src="https://fedoramagazine.org/wp-content/themes/fmag2018-1.02/images/fmag-ribbon.png">
		</a>
	</div>
</div>