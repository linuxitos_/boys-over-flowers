<!doctype html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?= base_url('public/assets/images/favicon.png'); ?>">
		<title><?=$title;?></title>

		<!-- Bootstrap core CSS-->
		<link href="<?= base_url('public/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
		<link href="<?= base_url('public/vendor/bootstrap-icons/bootstrap-icons.min.css'); ?>" rel="stylesheet">
		<link href="<?= base_url('public/vendor/bootstrap/css/animate.css'); ?>" rel="stylesheet">
		<link href="<?= base_url('public/vendor/bootstrap/css/animation.css'); ?>" rel="stylesheet">
		<link href="<?= base_url('public/vendor/bootswatch/bootstrap.min.css'); ?>" rel="stylesheet">
		<link href="<?= base_url('public/assets/css/custom.css'); ?>" rel="stylesheet">
		<link href="<?= base_url('public/vendor/fontawesome/css/all.min.css'); ?>" rel="stylesheet">
		
		
		<meta name="description" content="Tips sobre linux, Hacks, Tutoriales, Desarrollo Web e Ideas en un sólo Blog">
		<meta name="author" content="Fernando Merino">
		<meta name="keywords" content="linuxitos,fedora,development,blog,m2">
	</head>
<body>