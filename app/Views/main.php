<?php
include 'common/head.php';
include 'common/navbar.php';
include("mdls/mdl_posts.php");

?>

<script type='text/javascript'>
	var base_url = '<?= base_url(); ?>';
</script>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header pb-0 border-bottom-0">
					<div class="card-title">
						Mis momentos felices
					</div>
					<nav class="mt-2">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item mt-2" role="presentation">
								<a class="nav-link text-active-primary active" data-bs-toggle="tab" href="#nav-home">
									<i class="fa-thin fa-calendar-lines-pen"></i> <span class="d-none d-md-inline-block">Mis momentos</span>
								</a>
							</li>
							<li class="nav-item mt-2" role="presentation">
								<a class="nav-link text-active-primary" data-bs-toggle="tab" href="#nav-otp">
									<i class="fa-thin fa-shield-heart"></i> <span class="d-none d-md-inline-block">OTP code</span>
								</a>
							</li>
						</ul>
					</nav>	
				</div>
				<div class="card-body">
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab" tabindex="0">
							<div class="row">
								<div class="col-md-4 mb-2">
									<div class="card">
										<span for="" class="p-1 text-bg-danger rounded">
											Última flor recibida:
										</span>
										<ul id="ulCntPost" class="list-group">
											
										</ul>
									</div>
								</div>
								<div class="col-md-2 mb-2">
									<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
										<i class="bi bi-arrow-clockwise"></i> Actualizar
									</button>
								</div>
								<div class="col-md-6">
									<div class="card">
										<div class="card-header">
											<h2 class="card-title text-primary">
												¿Hace cuánto no recibes flores?:  <span class="badge rounded-pill bg-dark"><?=timeago($post->fc_post);?></span>
											</h2>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<hr>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 mb-2">
								<button type="button" class="btn btn-success float-end cleanFormAddFel" data-bs-toggle="modal" data-bs-target="#addFel">
										<i class="bi bi-plus-circle"></i> Agregar
									</button>
								</div>
							</div>
							<div id="divCntFels" class="row">
								
							</div>
						</div>
						<div class="tab-pane fade" id="nav-otp" role="tabpanel" aria-labelledby="nav-otp-tab" tabindex="0">
							<div class="row">
								<div class="col-md-12">
									<button type="button" class="btn btn-primary btnOpenMdlOtp" data-bs-toggle="modal" data-bs-target="#mdlOtp">
										<i class="bi bi-shield-check"></i> OTP
									</button>
										<!-- Modal -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php
include 'common/foot.php';
?>
<script>
	$(document).ready(function() {
		loadEvts();
		loadFels()
	});
</script>