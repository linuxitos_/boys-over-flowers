--
-- Database: login-auth-civ
--
DROP DATABASE IF EXISTS menflowers;
CREATE DATABASE IF NOT EXISTS menflowers DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;
USE menflowers;

-- --------------------------------------------------------

--
-- Table structure for table users
--

DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users (
	id_user int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	act_user tinyint(4) NOT NULL DEFAULT 1,
	fc_user datetime NOT NULL DEFAULT current_timestamp(),
	nom_user varchar(200) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
	firstname varchar(50) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
	lastname varchar(50) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
	email varchar(100) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
	password varchar(255) COLLATE latin1_spanish_ci NOT NULL DEFAULT '',
	created_at datetime NOT NULL DEFAULT current_timestamp(),
	updated_at datetime NOT NULL DEFAULT current_timestamp()
);

--
-- Table structure for table ajx_posts
--

DROP TABLE IF EXISTS posts;
CREATE TABLE IF NOT EXISTS posts (
	id_post int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	fc_post timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
	act_post tinyint(4) NOT NULL DEFAULT 1,
	nom_post varchar(512) COLLATE utf8_spanish_ci DEFAULT NULL,
	desc_post varchar(512) COLLATE utf8_spanish_ci DEFAULT NULL,
	img_post varchar(180) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'default_post.svg',
	slug_post varchar(120) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'none-s',
	type_post varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'post',
	user_id int(11) DEFAULT NULL,
	foreign key(user_id) references users(id_user) on update cascade on delete cascade
);



SELECT * FROM posts p ;
























