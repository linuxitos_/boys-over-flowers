var keyDown = false, ctrl = 17, vKey = 86, Vkey = 118;
 $(document).keydown(function (e) {
     if (e.keyCode == ctrl) keyDown = true;
 }).keyup(function (e) {
     if (e.keyCode == ctrl) keyDown = false;
 });
 $('.onlyNumbers').on('keypress', function (e) {
     if (!e) var e = window.event;
     if (e.keyCode > 0 && e.which == 0) return true;
     if (e.keyCode) code = e.keyCode;
     else if (e.which) code = e.which;
     var character = String.fromCharCode(code);
     if (character == '\b' && character == ' ' && character == '\t') return true;
     if (keyDown && (code == vKey || code == Vkey)) return (character);
     else return (/[0-9]$/.test(character));
 }).on('focusout', function (e) {
     var $this = $(this);
     $this.val($this.val().replace(/[^0-9]/g, ''));
 }).on('paste', function (e) {
     var $this = $(this);
     setTimeout(function () {
         $this.val($this.val().replace(/[^0-9]/g, ''));
     }, 5);
 });

function loadEvts() {
    $.ajax({
        url: base_url + "/loadEvts",
        method: "POST",
        dataType: "JSON",
        type: "POST",
        data: {
            user: 1,
        },
        beforeSend: function (objeto) {
            $("#ulCntPost").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Cargando...');
        },
        success: function (data) {
            $("#ulCntPost").html(data.results);
        },
        error: function (response) {
            //$("#ulCntPost").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Cargando...');
        }
    });
};

$(document).on("submit", ".formAddEvt", function (e) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        dataType: "JSON",
        method: "POST",
        url: base_url + "addEvt",
        data: parametros,
        beforeSend: function (objeto) {
            $("#btnAddEvt").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Guardando');
            $('#btnAddEvt').attr("disabled", true);
        },
        success: function (datos) {
            $("#btnAddEvt").html('<i class="bi bi-check-circle"></i> Actualizar');
            $('#btnAddEvt').attr("disabled", false);
            if (datos.type == "success") {
                $("#bntCloseAddEvt").trigger("click");
				loadEvts();
            }
            swal("Aviso", datos.msg, datos.type);
            $('.formAddEvt').trigger("reset");
        },
        error: function (data) {
            $("#btnAddEvt").html('<i class="bi bi-check-circle"></i> Actualizar');
            $("#btnAddEvt").attr("disabled", false);
            swal("Aviso", 'Error interno, revisa algo', 'error');
            $('.formAddEvt').trigger("reset");
        }
    });
    e.preventDefault();
});


$(document).on("submit", ".formAddFel", function (e) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        dataType: "JSON",
        method: "POST",
        url: base_url + "addFel",
        data: parametros,
        beforeSend: function (objeto) {
            $("#btnAddFel").html('<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Guardando');
            $('#btnAddFel').attr("disabled", true);
        },
        success: function (datos) {
            $("#btnAddFel").html('<i class="bi bi-check-circle"></i> Agregar');
            $('#btnAddFel').attr("disabled", false);
            if (datos.type == "success") {
                $("#bntCloseAddFel").trigger("click");
				loadFels();
            }
            swal("Aviso", datos.msg, datos.type);
        },
        error: function (data) {
            swal("Aviso", 'Error interno, revisa algo', 'error');
            $("#btnAddFel").html('<i class="bi bi-check-circle"></i> Agregar');
            $("#btnAddFel").attr("disabled", false);
        }
    });
    e.preventDefault();
});

function loadFels() {
    $.ajax({
        url: base_url + "/loadFels",
        method: "POST",
        dataType: "JSON",
        type: "POST",
        data: {
            user: 1,
        },
        beforeSend: function (objeto) {
            $("#divCntFels").html('<div class="alert text-center alert-dismissible alert-secondary">'+
                        '<button type="button" class="btn-close" data-bs-dismiss="alert"></button>'+
                        '<span class="spinner-border spin-x" role="status" aria-hidden="true"></span> Cargando...'+
                        '</div>');
        },
        success: function (data) {
            $("#divCntFels").html(data.results);
        },
        error: function (response) {
            $("#divCntFels").html('<p class="text-center">Sin felicitaciones agregadas</p>');
        }
    });
};


$(document).on("click", ".cleanFormAddFel", function () {
    $('.formAddFel').trigger("reset");
});

$(document).on("click", ".btnDelFel", function () {
	var fel = $(this).data('fel');

    swal({
        title: "¿Eliminar el registro?",
        content: '<i class="bi bi-question-circle"></i>',
        buttons: {
            cancel: {
                text: "Cancelar",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
            },
            eliminar: {
                text: "Eliminar",
                value: 'eliminar',
                visible: true,
                className: "",
                closeModal: true
            }
        },
    }).then((value) => {
        switch (value) {
            case "eliminar":
                $.ajax({
                    type: "POST",
                    url: base_url + "delFel",
                    dataType: "JSON",
                    data: {
                        fel: fel,
                    },
                    beforeSend: function (objeto) {
                        $("#btnDelFel"+fel).html('<div class="spinner-border spin-x" role="status" aria-hidden="true"></div>');
                        $('btnDelFel'+fel).prop('disabled', true);
                    },
                    success: function (datos) {
                        $("#btnDelFel"+fel).html('<i class="bi bi-trash-fill text-danger"></i>');
                        $('btnDelFel'+fel).prop('disabled', false);
                        if (datos.type == "success") {
                            swal("Aviso", datos.msg, datos.type);
                            loadFels()
                        }else{
                            swal("Aviso", 'Error interno, intenta más temprano.', 'error');
                        }
                    },
                    error: function (datos) {
                        $("#btnDelFel"+fel).html('<i class="bi bi-trash-fill text-danger"></i>');
                        $('btnDelFel'+fel).prop('disabled', false);
                    }
                });
                break;
        }
    });
});

    //$(window).on("load", function() {
$(document).on("click", ".btnOpenMdlOtp", function () {
    $('.formOtp').trigger("reset");
    //https://stackoverflow.com/questions/41698357/how-to-partition-input-field-to-appear-as-separate-input-fields-on-screen
    $("#divCntOtp").html('');
    $("#otp").removeClass('d-none');
    $("#btnNewCode").removeAttr('disabled');
    $('.txtOtp').removeAttr('readonly');
    function OTPInput() {
        const editor = document.getElementById('first');
        editor.onpaste = pasteOTP;
        const inputs = document.querySelectorAll('#otp > *[id]');
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener('input', function(event) {
                if(!event.target.value || event.target.value == '' ){
                    if(event.target.previousSibling.previousSibling){
                        event.target.previousSibling.previousSibling.focus();
                    }
                }else{
                    if(event.target.nextSibling.nextSibling){
                        event.target.nextSibling.nextSibling.focus();
                    }
                }
            });
        }
    }
    OTPInput();
});


function pasteOTP(event){
    event.preventDefault();
    let elm = event.target;
    let pasteVal = event.clipboardData.getData('text').split("");
    if(pasteVal.length > 0){
        while(elm){
            elm.value = pasteVal.shift();
            elm = elm.nextSibling.nextSibling;
        }
    }
}


$(document).ready(function() {
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        let stateObj = { id: "100" };
        window.history.replaceState(stateObj,"Page", e.target.hash);
    });
});
$('.dropdown-not-close').on('click', function(event) {
    event.stopPropagation();
});


$(document).on("keyup", ".txtOtp", function () {
    var complete = true;
    var code = "";
    $('.txtOtp').each(function(){
        if(this.value==""){
            complete=false;
        }else{
            code = code + ""+ this.value;
        }
    });
    if (complete){
        $('.txtOtp').attr('readonly', 'readonly');
        sendCode(code);
    }
});


function sendCode(code) {
    $.ajax({
        url: base_url + "sendCode",
        method: "POST",
        dataType: "JSON",
        type: "POST",
        data: {
            code: code,
        },
        beforeSend: function (objeto) {
            $("#otp").addClass('d-none');
            $("#btnNewCode").attr('disabled', 'disabled');
            $("#divCntOtp").html('<div class="text-center"><span class="spinner-border spin-x spin-2x" role="status" aria-hidden="true"></span> <br> validando ...</div>');
        },
        success: function (datos) {
            $("#divCntOtp").html('');
            if (datos.type == "success") {
                swal("Aviso", datos.msg, datos.type);
                $("#bntCloseMdlOtp").trigger("click");
            }else{
                $("#divCntOtp").html('');
                $("#otp").removeClass('d-none');
                $("#btnNewCode").removeAttr('disabled');
                swal("Aviso", 'Error interno, intenta más temprano.', 'error');
            }
        },
        error: function (response) {
            $("#divCntOtp").html('');
            $("#otp").removeClass('d-none');
            $("#btnNewCode").removeAttr('disabled');
        }
    });
};